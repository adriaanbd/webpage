class BoardMessageMailer < ApplicationMailer
  default from: 'floss-pa <do-not-reply@floss-pa.net>'

    def new_message(user,board_message)
      @user = user
      @message = board_message
      @message_user = User.find(@message.user_id)
      mail(to: "#{@user.name} <#{@user.email}>", subject: t(:user_left_a_message_the_board,
           user_name: @message_user.name, board_name: @message.board.community.name))
    end
end
