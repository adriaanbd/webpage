json.extract! badge, :uuid, :name, :description, :criteria, :expire, :tags, :claim_until, :created_at, :updated_at
json.url badge_url(badge, format: :json)
