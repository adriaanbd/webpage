class PreferencesController < ApplicationController
  before_action :set_preference
  def index
  end

  def update
    respond_to do |format|
     if @preference.update(preference_params)
       format.html {redirect_to preferences_url, notice: t(:preference_has_been_update)}
       format.js
     else
       format.htm {redirect_to preferences_url}
       format.js
     end
    end
  end
  private
  def set_preference
    @preference = current_user.preference
  end
  def preference_params
    params.require(:preference).permit(:email_event_invites,:email_notifications,:email_community_invites,:email_board_notifications)
  end
end
