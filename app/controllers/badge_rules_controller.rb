class BadgeRulesController < ApplicationController
  before_action :authenticate_user!
  before_action :set_badge_rule, only: [:show, :edit, :update, :destroy]

  # GET /badge_rules
  # GET /badge_rules.json
  def index
    @badge_rules = BadgeRule.all
  end

  # GET /badge_rules/1
  # GET /badge_rules/1.json
  def show
  end

  # GET /badge_rules/new
  def new
    @badge_rule = BadgeRule.new
  end

  # GET /badge_rules/1/edit
  def edit
  end

  # POST /badge_rules
  # POST /badge_rules.json
  def create
    @badge_rule = BadgeRule.new(badge_rule_params)

    respond_to do |format|
      if @badge_rule.save
        format.html { redirect_to @badge_rule, notice: t(:was_successfully_created) }
        format.json { render :show, status: :created, location: @badge_rule }
      else
        format.html { render :new }
        format.json { render json: @badge_rule.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /badge_rules/1
  # PATCH/PUT /badge_rules/1.json
  def update
    respond_to do |format|
      if @badge_rule.update(badge_rule_params)
        format.html { redirect_to @badge_rule, notice: t(:was_successfully_updated) }
        format.json { render :show, status: :ok, location: @badge_rule }
      else
        format.html { render :edit }
        format.json { render json: @badge_rule.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /badge_rules/1
  # DELETE /badge_rules/1.json
  def destroy
    @badge_rule.destroy
    respond_to do |format|
      format.html { redirect_to badge_rules_url, notice: t(:was_successfully_destroyed) }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_badge_rule
      @badge_rule = BadgeRule.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def badge_rule_params
      params.require(:badge_rule).permit(:name, :times, :inmediatly, :badgeble_type, :deliver)
    end
end
