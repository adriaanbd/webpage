class ConversationsController < ApplicationController

   def index
     #@users = User.where("confirmed_at is not null and id<>?",current_user.id)
     @conversation = Conversation.new
     @conversations = Conversation.where("user_id = ? OR recipient_id = ?", current_user.id, current_user.id)
     unless params[:id].nil?
       @messages = Conversation.find(params[:id]).messages
     end
   end

   def create
     if Conversation.between(current_user.id, params[:conversation][:recipient_id]).present?
       @conversation = Conversation.between(current_user.id, params[:conversation][:recipient_id]).first
       @message = @conversation.messages.create!(user_id: current_user.id, body: params[:body])
     else
       @conversation = Conversation.new(conversation_params)
       @conversation.user_id = current_user.id
       if @conversation.save
         @message = @conversation.messages.create!(user_id: current_user.id, body: params[:body])
       end
     end
     respond_to do |format|
        format.html {redirect_to conversation_messages_path(@conversation)}
        format.js
     end

   end
   def new
     @conversation = Conversation.new
     @message = @conversation.messages.new
     @recipient = User.find(params[:recipient_id])
     respond_to do |format|
       format.html
       format.js
     end
   end
   private
     def conversation_params
       params.require(:conversation).permit(:user_id, :recipient_id)
     end
end
