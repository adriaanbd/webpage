class Invite < ApplicationRecord
  belongs_to :community
  belongs_to :user
  has_many   :invitation_sends


end
