class InvitationSend < ApplicationRecord
  belongs_to :invite

  before_create :set_to_user, :set_uuid

  validate :domain_invalid?

  after_create :send_invitation
  after_create :create_notification

  private

  def set_to_user
    user = User.find_by_email(self.email)
    unless user.nil?
      self.to_user_id = user.id
    end
  end

  def set_uuid
    self.uuid = SecureRandom.uuid if self.uuid.nil?
  end

  def domain_invalid?
    begin
      domain = self.email.split("@").last
      s = Socket.gethostbyname(domain)
    rescue SocketError
      errors.add(:email, :invalid_domain, not_allowed: " email domain is invalid")
      return false
    end
  end

  def send_invitation
    InviteMailer.new_invite(self.invite.community,self).deliver_later
  end

  def recipients
    [User.find(self.to_user_id)]
  end

  def create_notification
    unless self.to_user_id.nil?
      recipients.each do |recipient|
        Notification.create(recipient: recipient, user: self.invite.user, notifiable: self.invite, action: 'invited')
      end
    end
  end
end
