class UserBadge < ApplicationRecord
  belongs_to :user
  belongs_to :badge
  validates :user, uniqueness: {scope: :badge}
  after_create :send_new_badge_notice

  def send_new_badge_notice
    unless self.user.email.include?('facebook')
      BadgeMailer.new_badge(self.user,self.badge).deliver_later
    end
  end
end
