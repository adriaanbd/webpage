class Contact < ApplicationRecord
  belongs_to :community
  belongs_to :user

  after_create :contact_by_emails

  private

  def contact_by_emails
    logger.info 'here'
    community = self.community
    if self.members=="all"
      members = community.members
    else
      members = community.members.where("admin=true")
    end
    logger.info members.inspect
    unless members.empty?
      members.each do |member|
        unless member.user.email.include?('facebook')
          ContactMailer.members(member.user,community,self).deliver_later
        end
      end
    end
  end
end
