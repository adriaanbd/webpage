require 'test_helper'

class ProfileViewsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @profile_view = profile_views(:one)
  end

  test "should get index" do
    get profile_views_url
    assert_response :success
  end

  test "should get new" do
    get new_profile_view_url
    assert_response :success
  end

  test "should create profile_view" do
    assert_difference('ProfileView.count') do
      post profile_views_url, params: { profile_view: { name: @profile_view.name } }
    end

    assert_redirected_to profile_view_url(ProfileView.last)
  end

  test "should show profile_view" do
    get profile_view_url(@profile_view)
    assert_response :success
  end

  test "should get edit" do
    get edit_profile_view_url(@profile_view)
    assert_response :success
  end

  test "should update profile_view" do
    patch profile_view_url(@profile_view), params: { profile_view: { name: @profile_view.name } }
    assert_redirected_to profile_view_url(@profile_view)
  end

  test "should destroy profile_view" do
    assert_difference('ProfileView.count', -1) do
      delete profile_view_url(@profile_view)
    end

    assert_redirected_to profile_views_url
  end
end
