class CreateBadges < ActiveRecord::Migration[5.1]
  def change
    create_table :badges do |t|
      t.bigint :owner_id, foreign_key: {to_table: :users}
      t.string :uuid, :null => false
      t.string :name
      t.string :description
      t.string :criteria
      t.datetime :expire
      t.string :tags
      t.datetime :claim_until

      t.timestamps
      t.index :uuid, unique: true
    end
  end
end
