class CreateInvites < ActiveRecord::Migration[5.1]
  def change
    create_table :invites do |t|
      t.references :community, foreign_key: true
      t.references :user, foreign_key: true
      t.integer :control
      t.integer :emails
      t.string  :file
      t.text    :email_list
      t.integer :send_emails, default: 0
      t.integer :send_notifications, default: 0
      t.timestamps
    end
  end
end
