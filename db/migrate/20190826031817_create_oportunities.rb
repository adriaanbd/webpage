class CreateOportunities < ActiveRecord::Migration[5.1]
  def change
    create_table :oportunities do |t|
      t.string :name

      t.timestamps
    end
  end
end
