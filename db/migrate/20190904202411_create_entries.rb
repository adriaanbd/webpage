class CreateEntries < ActiveRecord::Migration[5.1]
  def change
    create_table :entries do |t|
      t.references :feed, foreign_key: true
      t.string :title
      t.text :content
      t.string :url
      t.string :author
      t.datetime :published

      t.timestamps
    end
  end
end
