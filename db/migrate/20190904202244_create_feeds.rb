class CreateFeeds < ActiveRecord::Migration[5.1]
  def change
    create_table :feeds do |t|
      t.references :user, foreign_key: true
      t.string :name, limit: 75
      t.string :url
      t.text :description

      t.timestamps
    end
  end
end
