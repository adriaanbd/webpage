class CreatePreferences < ActiveRecord::Migration[5.1]
  def change
    create_table :preferences do |t|
      t.references :user, foreign_key: true
      t.boolean :email_community_invites, default: true
      t.boolean :email_board_notifications, default: true
      t.boolean :email_event_invites, default: true
      t.boolean :email_notifications, default: true
      t.references :profile_view, foreign_key: true
      t.references :oportunity, foreign_key: true

      t.timestamps
    end
  end
end
