class AddUserBadgesUniqueIndex < ActiveRecord::Migration[5.1]
  def change
    add_index :user_badges, [:user_id,:badge_id], unique: true
  end
end
