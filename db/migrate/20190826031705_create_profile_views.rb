class CreateProfileViews < ActiveRecord::Migration[5.1]
  def change
    create_table :profile_views do |t|
      t.string :name

      t.timestamps
    end
  end
end
