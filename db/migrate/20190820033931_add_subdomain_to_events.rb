class AddSubdomainToEvents < ActiveRecord::Migration[5.1]
  def change
    add_column :events, :subdomain, :string, limit: 65, unique: true
  end
end
